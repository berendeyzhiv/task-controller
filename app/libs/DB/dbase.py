# mypy ignore lines count: 3
"""
This module works with Database.
"""
# import atexit as _atexit
import logging as _logging
from typing import (
    Final as _Final, 
    final as _final,
    Union as _Union,
    Type as _Type,
)

from libs.DB.configurate import (            # type: ignore
    ClientTable as _Table,
    Session as _Session,
)


@_final
class DataBase:
    """Interacting for a database."""

    def __init__(self, config: dict) -> None:
        """Create a new DataBase instance."""
        self._logger = _logging.getLogger(__name__)
        self._orm: _Type[_Table] = _Table
        self.__cfg = config  # no used yet

        self._session = _Session()

    @property
    def orm(self): return self._orm

    @property
    def session(self): return self._session
    
    def save(self, *args, **kwargs) -> _Union[bool, Exception]:       # type: ignore
        """Save data to database."""
        self._logger.debug(f'  Saving to database {args=} ...')

        try:
            data = DataBase._clean_data(*args, **kwargs)
        except ValueError as ve:
            self._logger.exception(ve)
        else:
            always = data['success']
            assert always, 'The adbyss looks into you'

            cleaned_data: Final[str] = data['cleaned']
            return self._insert(cleaned_data)

    @staticmethod
    def _clean_data(*args, **kwargs) -> dict:
        """Clear data to save it."""
        data, = args
        # ... validation ...
        kwargs |= {'cleaned': data, 'success': True, 'dirty': []}
        return kwargs

    def _insert(self, __cleaned: str, /) -> _Union[bool, Exception]:       # type: ignore
        """Insert data into database.
           
           return True if successfully or raise Exception

        """
        try:
            SUCCESSFULLY = True
            ins = self._orm(__cleaned)
            self._session.add(ins)
            self._session.commit()
            self._logger.debug(f'  Inserted done!')

            return SUCCESSFULLY
        except Exception as ext:
            self._logger.exception(ext)

    def _select(self, what=None, from_=None):
        select_all = what is from_ is None
        if select_all:
            return self._session.query(_Table).all()
        else:
            pass

    # @atexit.register
    # def _in_the_end(self) -> None:
    #     self._logger.info(f'  Something all-important')
 