# mypy ignore lines count: 8
"""
Module designed to make creating and sending emails
using the built-in smtplib package.

Classes
-------
EmailSender
    Sending emails with Python’s built-in smtplib library

"""
import logging as _logging
import smtplib as _smtplib
from smtplib import SMTP as _SMTP
from typing import Final as _Final, final as _final
from collections.abc import ValuesView as _dict_values


@_final
class EmailSender:
    """
    Notifier. ​Create and send emails.

    Methods
    -------
    `notify(args, kwargs)`
        Interaction between client and internal code

    """
    def __init__(self, configs: dict) -> None:
        """
        Initialize a new EmailSender instance.

        Parameters
        ----------
        configs : dict  
            keys: smtp, login, pwd, lvl, subj

        """
        self._logger = _logging.getLogger(__name__)
        try:
            self._smtp   :  _Final[str] = _SMTP(configs.get('smtp'))   # type: ignore
            self._login  :  _Final[str] = configs.get('login')         # type: ignore
            self._passwd :  _Final[str] = configs.get('pwd')           # type: ignore
            self._debug  :  _Final[int] = configs.get('lvl')           # type: ignore
            self._subj   :  _Final[str] = configs.get('subj')          # type: ignore
        except KeyError as ke:
            self._logger.exception(f'  Config parameters are missing', ke)

    @property
    def smpt(self): return self._smtp

    @property
    def login(self): return self._login

    @property
    def password(self): return self._passwd

    def notify(self, *args, **kwargs) -> bool:
        """
        Return True if mailing done else False.

        Parameters
        ----------
        args : tuple[dict_values, str], (passed through)  
        kwqrgs : dict[Any, Any],  (passed through)  

        Calls
        -----
        `_send_emails(args, kwargs)` -> bool[success]

        """
        self._logger.info('  Trying to send mails ...')
        return self._send_emails(*args, **kwargs)

    def _send_emails(self, *args: tuple[_dict_values, str], **kwargs) -> bool:
        """
        Send the messages. Return  `True`  if successful else  `False`.

        First format the recipients list  
        then connect to the SMTP server and authenticate  
        finally emailing them.

        Parameters
        ----------
        args : tuple[dict_values, str]
            mailing list,  # contains also bcc-recipients list
            message
        kwargs : dict[Any, Any], optional

        Calls
        -----
        `_generate_message(args)` -> str msg template  
        `_smtp(_login, _passwd, _debug)` -> None

        Raises
        ------
        `_smtplib.SMTPAuthenticationError`
            Server is not available
        `_smtplib.SMTPException`
            Any smpt exception
        `OSError`
            Windows problem

        """
        mailing_list, message = args

        FROM: str  = self._login                                      # type: ignore
        TO  : list = sum(mailing_list, [])                            # type: ignore
        BODY: str  = self._generate_message(*args)

        try:
            with self._smtp as server:                                # type: ignore
                server.ehlo()
                server.starttls()
                server.login(self._login, self._passwd)
                server.set_debuglevel(self._debug)
                server.sendmail(FROM, TO, BODY)
            
            self._logger.info('  Mailing successfully finished.')

            return True
        except _smtplib.SMTPAuthenticationError as aue:
            self._logger.exception(aue)
        except _smtplib.SMTPException as smtpe:
            self._logger.exception(smtpe)
        except OSError as oe:
            self._logger.exception(oe)
        return False

    def _add_attachments(self):
        """Add required attachments."""
    
    def _generate_message(self, *args) -> str:
        """
        Put the parts of the email together.

        Parameters
        ----------
        args : tuple(dict_values(list), str)
            (to, cc, bcc:not used), message
        
        Returns
        -------
            From: from 
            To: to 
            CC: cc 
            Title: subj 
            message body

        """
        (to, cc, _bcc), message = args

        return '\r\n'.join(
            (
                f'From: {self._login}',
                f'To: {", ".join(to)}',
                f'CC: {", ".join(cc)}',
                f'Title: {self._subj}',
                f'\n{message}'
            )
        )
