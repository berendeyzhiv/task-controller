# soap, excel, email, database

appdirs==1.4.4
attrs==20.3.0
cached-property==1.5.2
certifi==2020.12.5
chardet==4.0.0
defusedxml==0.7.1
et-xmlfile==1.0.1
greenlet==1.0.0
idna==2.10
isodate==0.6.0
lxml==4.6.3
numpy==1.20.2
openpyxl==3.0.7
pandas==1.2.3
psycopg2==2.8.6
python-dateutil==2.8.1
pytz==2021.1
requests==2.25.1
requests-file==1.5.1
requests-toolbelt==0.9.1
six==1.15.0
sqlalchemy==1.4.6
urllib3==1.26.4
zeep==4.0.0