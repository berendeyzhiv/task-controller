# Будут иметь место события и подписка на них
# При вызове отработают функции, прослушивающие события 
# __isub__, __imul__ no usage yet
import logging


class Event:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.__handlers = []

    def __call__(self, *args, **kwargs):
        for f in self.__handlers:
            f(*args, **kwargs)

    def __iadd__(self, handler):
        self.__handlers.append(handler)
        self.logger.debug(f'{self.__handlers=}')
        return self

    def __isub__(self, handler):
        self.__handlers.remove(handler)

    def __imul__(self, handler):
        self.__handlers * handler

    def __repr__(self):
        return (f'{self.__class__.__name__}('
                f'{self.__handlers=!r})')

    def clear(self):
        self.__imul__(0)
        return self
