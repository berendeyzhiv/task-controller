_major = '0'
_minor = '0'
_micro = '1'

# Получить версию как кортеж
__version_info__ = _major, _minor, _micro

# Стандартно как строка 
__version__ = '.'.join(__version_info__)

# Надо, нет?
__status__  = 'dev'

__date__    = 'xx-xx-xxxx'
 