# если пакет подключен 
  # Обратиться к SOAP сервису, если модуль подключен
  # сохранить полученные данные в бд, если модуль подключен и ответ содержит данные
  # получить контакты сотрудников из Excel, если модуль подключен
  # отправить данные на имейлы сотрудников, если данные из Excel получены

# Настройки параметров в `settings.base` 
"""
===========================================
Package for interaction between SOAP client 
and the participants of the email campaign.
===========================================

"""
from .Logic import Lead as main

__all__ = main,

"""
tree .
    .                        ## CORE DIRECTORY (libs) - - - - -
    ├── client.py               # SOAP client interface
    ├── DB                      ## DATABASE DIRECTORY - - - - -
    │   ├── configurate.py          # ORM, URL configuration
    │   └── dbase.py                # Database requestor
    ├── excel.py                # Pandas read Excel
    ├── __init__.py             # Package __doc__, Lead
  > ├── Logic.py                # Main module
    ├── mail.py                 # SMTP email sending
    └── meta.py                 # Utils of this subpackage

1 directories, 8 files


-------------
requirements:
    SOAP  -- zeep
    db    -- sqlalchemy, psycopg2
    Excel -- pandas, openpyxl
    mail  -- smtplib

"""                   
