# mypy ignore lines count: 4
# Работает напрямую с подключаемыми функциональностями(например  `libs`).
# Протоколу ничего неизвестно о пакетах, которые будут импортированы,
# но ожидается, что пакет содержит объект  `main`, имеющий метод  `run()`.
"""
This module works with subpakcages connected to it
and sets the procedure for working with them.

Classes
------- 
OperationalStatus(Enum)
    Contains two attributes: FINICHED, FAULTED
Protocol
    Interaction protocol with functionality(plug-in packages)

"""
import logging as _logging
import threading as _threading
import time as _time
from enum import Enum as _Enum
from types import SimpleNamespace
from typing import ClassVar as _ClassVar, Optional as _Optional

from event import Event as _Event     


class OperationStatus(_Enum):
    """Pass status to `Event().__handlers`"""
    FINISHED = 0
    FAULTED = 1


class Protocol:
    """
    Interaction protocol with  `_FUNCTIONALITY`  list.
    
    Methods
    -------
    `send(args, kwargs)` : Start all enabled functionality. 
      
    """
    _FUNCTIONALITY: _ClassVar[set[object]] = set()

    def __init__(self, configs: SimpleNamespace) -> None:
        """
        Initialize a new Protocol instance.
        
        Parameters
        ----------
        configs : SimpleNamespace  

        Attributes
        ----------
        `_logger` : __name__  
        `_FUNCTIONALITY` : set
            Dynamically imported pakcages from configs.packages
        `disable_process` : bool=False
            Disable all functionality interaction if True
        `message_received()`
            A new Event instance

        """
        self._logger = _logging.getLogger(__name__)

        for package in configs.packages:
            self._FUNCTIONALITY.add(
                __import__(package).main(configs)
            )

        self.disable_process: bool  = configs.off.get('all', False)
        self.message_received: _Event = _Event()

    def send(self, *args, daemon=True, **kwargs) -> None:
        """
        Start all enabled functionality.

        Parameters
        ----------
        args : tuple(soap_arg,)  
        daemon : bool=True, background  
        kwargs : dict(wsdl=soap_server_url)

        See Also
        --------
        `process_sending()` inner function
        
        """      
        def process_sending() -> None:
            if self.disable_process:
                self._logger.info('  OFF: Disabled libs subpackage')
                return self.message_received(OperationStatus.FINISHED)

            status_code: OperationStatus = self._process(*args, **kwargs)
            self._logger.info(f'  Operaion with an action with {args=}')
            self.message_received(status_code)
        
        thread = _threading.Thread(target=process_sending, daemon=daemon)
        thread.start()

    def _process(self, *args, **kwargs) -> OperationStatus:
        """
        For each package call  `main.run()`  and  
        Return  `OperationStatus.<FINISHED=0 or FAULTED=1>`

        Parameters
        ----------
        args : tuple(soap_arg,)  
        kwargs : dict(wsdl=soap_server_url)
           
        """
        self._logger.debug(f'  Processing operation with {args=}')
        # FIXME change next(inter(list))
        main: object = next(iter(self._FUNCTIONALITY))
        """`main`  must be a callable object with  `run()`"""
        op_code: _Optional[int] = main.run(*args, **kwargs)            # type: ignore

        _time.sleep(3)

        finished: bool = op_code == 1
        return OperationStatus.FINISHED if finished else OperationStatus.FAULTED
