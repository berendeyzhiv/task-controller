# mypy ignore lines count: 3
"""
This module works with Excel using pandas.

Classes:
-------
ExcelReader : Read and return Excel data

"""
import logging as _logging
from collections.abc import ValuesView as _dict_values
from pathlib import PosixPath as _PosixPath
from typing import (
    Optional as _Optional,
    final as _final,
    Any as _Any,
)

import pandas as _pd  # type: ignore

_T1 =  _Optional[_dict_values[list[dict]]]
_T2 = _Optional[_pd.DataFrame]


@_final
class ExcelReader:
    """
    Read and return Excel data.
       
    Methods:
    -------
    `take_contacts()`
        Interaction between client and internal code

    """
    _file   : _Optional[_PosixPath]
    _engine : _Optional[str]
    _header : _Optional[str]

    def __init__(self, configs: dict[str, _Any]) -> None:
        """
        Initialize a new ExcelReader instance. 

        Parametrs
        ---------
        configs : dict  
            {file=excel_url, engine=engine, header=header}

        """
        self._logger = _logging.getLogger(__name__)
        self._file   = configs.get('file')
        self._engine = configs.get('engine')
        self._header = configs.get('header')

    def take_contacts(self) -> _T1:
        """
        Return generated mailing list or None

        Calls
        -----
        `_send_pandas_to_excel(None)` -> Optional[DataFrame]
        `_gen_mailing_list(DataFrame)` -> Optional[dict_values]

        """
        df_employees = self._send_pandas_to_excel()
        if df_employees is None: return
        return self._gen_mailing_list(df_employees)

    def _gen_mailing_list(self, elist: _pd.DataFrame) -> _T1:
        """
        Generate general mailing list (to, cc, bcc).

        Parameters
        ----------
        elist : DataFrame

        Raises
        ------
        `AttributeError`
            If attribute does not exist
        `KeyError`
            (pass) If column does not exist
           
        """
        self._logger.debug(f'  Pandas descend from the eucalyptus ...')

        recipients: dict[str, list] = {'to': [], 'cc': [], 'bcc': []}

        try:
            for name, email, category in elist.values:
                recipients[category].append(email)

        except AttributeError as ae:
            self._logger.exception(ae)
        except KeyError:
            pass
        # Only values, Keys no usage yet
        return recipients.values()                               # type: ignore # dynamic

    def _send_pandas_to_excel(self) -> _T2:                      # type: ignore
        """
        Return Excel data using Pandas. 
        
        Raises
        ------
        `FileNotFoundError`
            File problem
        `TypeError`
            header problem
        `ValueError`
            engine problem
        `Exception`  (others)
           
        """
        try:
            df_employees = _pd.read_excel(
                self._file, header=self._header, engine=self._engine
            )
        except FileNotFoundError as fe:
            self._raise_current_exception(fe, 'file')
        except TypeError as te:
            self._raise_current_exception(te, 'header')
        except ValueError as ve:
            self._raise_current_exception(ve, 'engine')
        except Exception as e:
            self._raise_current_exception(e, '')
        else: 
            return df_employees

    def _raise_current_exception(self, ext: Exception, var: str) -> None:
        """
        Raise possible problem location depending on the exception.

        Parameters
        ----------
        ext : Exception
            Suitable exception
        var : str
            Possible problem location
        
        """
        self._logger.exception(f'  Check {var} here:'
                               f'  settings.base.CONFIGS.ex_conf.{var}',
                               ext.args
            )
 