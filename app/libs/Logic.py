# mypy ignore lines count: 0
"""
Main module for working with other 
(client, dbase, excel, mailer) components.

Classes
-------
Lead
    Main logic with other modules

"""
import logging as _logging
from types import SimpleNamespace
from typing import Any as _Any, final as _final
from collections.abc import ValuesView as _dict_values 

from libs.meta import ProcessOrdSetter as _OrdSetter
from libs.client import SOAPClient as _SOAPClient
from libs.DB.dbase import DataBase as _DataBase           
from libs.excel import ExcelReader as _ExcelReader           
from libs.mailer import EmailSender as _EmailSender


@_final
class Lead:
    """
    Basic logic to work other modules for receiving and 
    transmitting data from SOAP client to recipients.

    Methods
    -------
    run(args, kwargs)`
        Interaction between client and internal code

    See Also
    --------
    `meta.ProcessOrdSetter`
    
    """
    def __init__(self, configs: SimpleNamespace) -> None:
        """
        Initialize a new Lead instance.

        Parameters
        ----------
        configs : SimpleNamespace  
        configs.off : dict  
        configs.wsdl : str  
        configs.db_conf : dict  
        configs.ex_conf : dict  
        configs.smtp : dict

        """
        self._logger  = _logging.getLogger(__name__)
        self._util    = _OrdSetter(configs.off, self)               
        self._soap    = _SOAPClient(configs.wsdl_url)
        self._db      = _DataBase(configs.db_conf)
        self._exceler = _ExcelReader(configs.ex_conf)         
        self._emailer = _EmailSender(configs.smtp)        

    @property
    def util(self): return self._util

    @property
    def soap(self): return self._soap

    @property
    def db(self): return self._db

    @property
    def exceler(self): return self._exceler

    @property
    def emailer(self): return self._emailer

    def run(self, *args, **kwargs) -> bool:    
        """
        Start each component allowed by `_util`.

        Set  `_util.excel_ok`  as  `True`(data was taken) or  `False`.  
        return `True` if all ok or raise `Exception` due to unknown error.

        Parameters
        ----------
        args (Any), passed throuth   
        kwargs {Any}, passed throuth  

        Calls
        -----
        `_util.start_of_processes()` -> Iterator[func]  
        `_util.start_component(func, args, kwargs)` -> Any

        Raises
        ------
        `Exception` unknown
        
        """
        try:
            for func in self._util.set_order_of_processes().values():
                data: _Any = self._util.start_component(func, *args, **kwargs)
                self._util.excel_ok = isinstance(data, _dict_values)

                self._logger.debug('  Component response: '
                    f'{data if isinstance(data, (str, int)) else type(data)}')
                
                if not self._util.check_bool_tmp(data): return False       
            return True
        except Exception as e:
            self._logger.exception('  WTF Something bad happened!', e)

    def _x_get_response_soap(self, *args, **kwargs) -> str:
        return self._soap.request(*args, **kwargs)

    def _x_save_data_to_db(self, *args, **kwargs) -> bool:
        return self._db.save(*args, **kwargs)

    def _x_get_data_excel(self, *args, **kwargs) -> _dict_values:
        return self._exceler.take_contacts()

    def _x_send_emails(self, *args, **kwargs) -> bool:
        return self._emailer.notify(*args, **kwargs)
    