# mypy ignore line count: 1
# Устанавливает порядок выполнения включенных модулей в соответствии с
# флагами активности в `settings.CONFIGS`:
#           Некоторые  Logic.Lead.<методы>  имеют префикс '_x_'  
#           По ним через  meta.ProcessOrdSetter
#           ОПРЕДЕЛЯЕТСЯ ПОСЛЕДОВАТЕЛЬНОСТЬ ВЫЗОВОВ в  Logic.Lead.run
#           Таким образом, если флаг  `_mail_off`  включен,  '_x_метод'  не будет вызван
"""
Logic's util.

Classes
-------
ProcessOrdSetter
    Processes excecution order setter

"""
from typing import (
    Callable as _Callable,
    Union as _Union,
    final as _final,
    Any as _Any,
)
from collections.abc import (
    ValuesView as _ValuesView,
    ItemsView as _ItemsView
)

_CACHE = _Union[str, bool, _ValuesView[list[list, ...]]]


@_final
class ProcessOrdSetter:
    """
    Setter of the excecution order of the allowed components.

    Methods
    -------
    `set_order_of_processes()`
        Return list of dicts(flag: state)
    `start_component(func, args, kwargs)`
        Call allowed components and return cache
    `check_bool_tmp(_)`
        Check and return boolean _tmp

    """
    def __init__(self, configs: dict[str, bool], lead: _Callable) -> None:
        """
        Initialize a new PrpcessesOrdSetter instance.
        
        Parameters
        ----------
        configs : dict[str, bool]  
        lead : Callable (`Logic.Lead` instance)
                   
        Attributes
        ----------
        _enabled : configs
            `settings.base.CONFIGS`
        excel_ok : bool
            Data was taken or not
        _logic : Callable, `lead`
        _tmp : Any
            temporary data of each component

        """
        self._enabled  : dict   = configs
        self._excel_ok : bool   = False
        self._logic    : object = lead
        self._tmp      : _CACHE = False

    @property
    def excel_ok(self): return self._excel_ok

    @excel_ok.setter
    def excel_ok(self, data: bool): self._excel_ok = data

    def set_order_of_processes(self) -> list[dict[str, _Callable]]:
        """
        Set order of calling methods to starting processes in them.

        Calls
        -----
        `_get_methods_startswith_x()` -> list[Callable]  
        `_get_disabled_flags()` -> list[dict]

        Returns
        -------
        list[dict[`False`-flag: `Logic.Lead.<_x_method>()`], ...]
            
        """
        x_methods: list[_Callable] = [
            x for x in self._get_methods_startswith_x()]

        false_flags: list[str] = [
            k for d in self._get_disabled_flags() for k in d]

        flag_as_key__method_as_value = {}  # type: ignore # dict[str, Callable]

        for ff in false_flags:
            flag_as_key__method_as_value.update(*[
                {ff:m} for m in x_methods if m.__name__.endswith(ff)])
        #                                                ^  _x_method_<flag>

        return flag_as_key__method_as_value
    
    def start_component(self, func: _Callable, /, *args, **kwargs) -> _CACHE:
        """
        To successful start mailer component, redefine  `args`  as 
        (dict_values, *args,) if  `excel_ok`  is  `True`.  
        Redefine  `_tmp`  result of each component and returns it.

        Parameters
        ----------
        func : Callable  
        args : tuple(soap_arg,) or tuple(dict_values, soap_arg)  
        kwargs : dict('wsdl': 'soap_server_url')  

        Calls
        -----
        `func(args, kwargs)` -> Any

        Returns
        -------
        `_tmp` : Any, cache

        """
        if self._excel_ok: args = [self._tmp, *args]
        self._tmp = func(*args, **kwargs)

        return self._tmp

    def check_bool_tmp(self, _: _Any) -> bool:
        return bool(_ if not isinstance(_, _ValuesView) else sum(_, []))                 

    def _get_methods_startswith_x(self) -> list[_Callable]:
        """
        Return all  `Logic.Lead`  methods with  `_x_`  prefix.
        
        """
        def _(m): return m.startswith('_x_')
        return [getattr(self._logic, m) for m in dir(self._logic) if _(m)]

    def _get_disabled_flags(self) -> list[dict[str, bool]]:
        """
        Return all  `False`  flags from  `_enabled`.
        
        """
        flags: _ItemsView = self._enabled.items()
        return [{flag:state} for flag, state in flags if not state]
        # FIXME:        ^ state not used (return list[flags])
 