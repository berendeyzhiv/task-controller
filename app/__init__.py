"""
Welcome to the assignable functionality app!
--------------------------------------------

Getting started with  
  * The application expexts  `main`  object to exist in the  
    <package>.__init__.py  with  `run()`  method.  
  * Set 'main', 'run' as aliases if no match.  

To connect new functionality do next:  
  * add the main package into  `settings._FUNCTIONALITY`  
  * import  `CONFIGS.TERMINAL.PROTOCOL.DB`  into db module if db exists  

Usage
-----
  start  `main.py`  module with one argument <number_of_threads> (can be omitted)  
  (.venv) /../client_employees/app# ./main.py number_of_threads  


Vadim Vadimov [https://gitlab.com/dashboard/projects]  
License: None  

"""
from _version import __version__

__author__ = 'fj-fj-fj'

__email__  = 'berendeyzhiv@gmail.com'

__catchphrase__ = '<-- let\'s use it :)'

"""
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
tree  .
      .
      ├── app                       ## CORE DIRECTORY - - - - - - - - - -
      │   ├── event.py                 # events (threading.Thread)
      │   ├── exceptions               ## CUSTOM EXCEPTION DIRECTORY - -
      │   │   └── errors.py               # Custom ExceptionErrors
      │   ├── __init__.py              # __doc_version_email_author__
    > │   ├── main.py                  # START here: The main app module
      │   ├── protocol.py              # Subpackage protocol
      │   ├── settings                 ## SETTINGS DIRECTORY - - - - - -
      │   │   ├── base.py                 # Settings module
      │   │   ├── misc.py                 # .secrets os_environ-like
      │   │   └── users.py                # groups and users
      │   ├── terminal.py              # Access to the protocol
      │   └── _version.py              # Versioning
      └── requirements.txt           # 28 dependencies

1 directories, 11 files

"""
 