# mypy ignore lines: 8

from sqlalchemy import create_engine                        # type: ignore
from sqlalchemy.orm import sessionmaker                     # type: ignore
from sqlalchemy.engine.base import Engine                   # type: ignore
from sqlalchemy.pool import SingletonThreadPool             # type: ignore
from sqlalchemy.ext.declarative import declarative_base     # type: ignore
from sqlalchemy.ext.declarative import DeclarativeMeta      # type: ignore
from sqlalchemy import Column, Integer, String

from settings.base import CONFIGS as __                     # type: ignore


class DBConfig:
    """Configure database URL"""
    def __init__(
            self,
            dialect='sqlite',
            driver=None,
            username=None,
            password=None,
            host=None,
            port=None,
            database=None
        ):
        """Initialize a new DBConfig instance"""
        self.dialect  = dialect
        self.driver   = driver
        self.username = username
        self.password = password
        self.host     = host
        self.port     = port
        self.database = database

    def __str__(self):
        """If an emply dict is passed: creaturn SQLite URL  
           if correct data in dict: creaturn postgresql URL

        """
        sqlite_url = 'sqlite:///client_data.db'
        postgres_url = '{}+{}://{}:{}@{}:{}/{}'

        return sqlite_url if self.dialect == 'sqlite' else (
            postgres_url.format(*locals().values())
        )

try:
    _DB_URL = str(DBConfig(**__.db_conf))
except TypeError as te:
    from exceptions.errors import UnexpectedKeywordArgumentError  # type: ignore
    raise UnexpectedKeywordArgumentError(
        "Check URL here: settings.base.CONFIGS.TERMINAL.PROTOCOL.DB"
    ) from te

_ENGINE: Engine = create_engine(_DB_URL, poolclass=SingletonThreadPool)

_Base: DeclarativeMeta = declarative_base()


class ClientTable(_Base):
    __tablename__ = 'client_data'

    id = Column(Integer, primary_key=True)
    data = Column(String)

    def __init__(self, data):
        self.data = data

    def __repr__(self):
        return f'<{type(self).__name__}({self.data!r})>'


_Base.metadata.create_all(_ENGINE)

Session: sessionmaker = sessionmaker(bind=_ENGINE)
 