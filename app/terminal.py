# mypy ignore lines count: 3
# Верхняя граница приложения; Здесь происходит авторизация пользователя.
# Создается экземпляр Протокола с объектом `CONFIGS` для дальнейших конфигураций.
# Добавляются обработчики события, созданного в Протоколе.
"""
Classes
-------
Terminal : Working with the  `Protocol`

"""
import logging as _logging
import threading as _threading
from types import SimpleNamespace
from typing import ClassVar as _ClassVar

from exceptions.errors import AuthorizationError as _AError                  # type: ignore
from protocol import Protocol as _Protocol


class Terminal:
    """
    Terminal authorizes the user and connects to the  `Protocol`.

    Methods
    -------
    `start(kwargs)`
        work with the Protocol
    
    """
    _PROMPT: _ClassVar[str] = 'IN :: '

    def __init__(self, configs: SimpleNamespace) -> None:
        """
        Initialize a new Terminal instance.

        Parameters
        ----------
        configs : SimpleNamespaces
        configs.terminal.root, config.terminal.admin : dict authtorization

        Attributes
        ----------
        `_logger` : __name__
        `__set_authorize_or_exit(root, admin)`
            pre-autorization
        `_protocol(configs)`
            create a new `Protocol` instance with passing `CONFIGS`
        `_operation_signal()`
            create a new `threading.Event` instance
        `_protocol.message_received` : list[funcs]
            Add an `threading.Event` handlers

        """
        self._logger = _logging.getLogger(__name__)
        self._set_authorize_or_exit(configs.terminal.root, configs.terminal.admin)

        self._protocol = _Protocol(configs)    
        self._operation_signal = _threading.Event()                        
        self._protocol.message_received += self._on_message_received
        
    def start(self, **kwargs) -> _threading.Thread:
        """
        Terminal session. Wait output and excecute commands.

        Parameters
        ----------
        kwargs : tuplr(WSDL-URL,)
        return `_start_thread()`

        """
        try:
            args: tuple[str] = input(f'Enter context\n{Terminal._PROMPT}'),
        except KeyboardInterrupt:
            pass # this unlogin or open somethig
        return self._start_thread(*args, **kwargs)
    
    @staticmethod
    def _set_authorize_or_exit(*auth):
        # type: (tuple[dict, dict]) -> None  # or raise AuthorizationError
        """
        Wait input and compare it to the `config.ROOT` or `config.ADMIN`.

        Allow to continue working with the  `terminal`  with the
        correct data entered, else raise  `AuthorizationError`.
        
        """
        p: str = Terminal._PROMPT
        login: bool = input(p) in (d['login'] for d in auth)                # type: ignore    
        passwd: bool = input(p) in (d['password'] for d in auth)            # type: ignore

        if not all((login, passwd)):
            raise _AError('Login or password is not correct !')
        
    def _on_message_received(self, op_status: int) -> None:
        """
        Add a `threading.Event()`  to  `Event.__handlers`.
        
        """
        self._logger.info(f'  Signaling for event: {op_status=}')
        self._operation_signal.set()

    def _start_thread(self, *args, daemon=True, **kwargs) -> _threading.Thread:
        """
        Start  `Protocol`.

        Parameters
        ----------
        args : tuple(wsdl_url,)  
        daemon : bool=True, background  
        kwargs : dict(wdls=soap_server_url)

        Calls
        -----
        `process_start()` -> None
            inner function is a target of Thread

        See Also
        --------
        `process_start()` inner funcion

        """
        def process_start():
            self._protocol.send(*args, **kwargs)
            self._operation_signal.clear()
            self._logger.debug('  Waiting for signal')
            self._operation_signal.wait()
            self._logger.debug('  finished')

        thread = _threading.Thread(target=process_start, daemon=daemon)
        thread.start()

        return thread
