# # mypy: ignore-errors
# This file collects all required settings that are needed
"""
All application settings are here. (export `CONFIGS` only)

"""
__all__ = 'CONFIGS',

import os
import sys as _sys
import logging as _logging
from pathlib import Path as _Path
from typing import Callable as _func, Union as _Union
from types import SimpleNamespace as _SimpleNamespace
from types import MappingProxyType as _MappingProxyType

from settings.users import _root, _admin

# LOGGING
_logging.basicConfig(level=_logging.DEBUG)

for log in 'zeep', 'urllib3', 'sqlalchemy.engine':
    _logging.getLogger(log).setLevel(_logging.WARNING)


# COMAND LINE ARGUMENTS
# Optional argument: number of Threads
try: _ARGUMENT = int(_sys.argv[1])
except IndexError: _ARGUMENT = 0


# PATHs, URLs
_BASE_PATH = _Path(__file__).resolve().parent.parent
# add excel path to pass libs.mailer
_EXCEL_PATH = _BASE_PATH.joinpath('employees.xlsx')
# WSDL server url to pass libs.client
_WSDL_URL = os.getenv('SOAP_SERVER')


# add new functionality here
_FUNCTIONALITY = frozenset(('libs',))
"""Contains pakcages to run them."""


# set True to disable all functionality
# флаги отключения пакета и отдельных модулей
_OFF = {
    'all'   : (False),
    # DISABLE  `libs`  FLAG COMPONENTS
    # Возможность отключить отправку почты, сохранение в базу и т.д.
    # импликация модулей `libs.meta._ProcessOrdSetter.set_order_of_processes(): false_flags`
    'soap'  : (False),
    'db'    : (False),
    'excel' : (False),
    'email' : (True),
}
# add disable flag components also for your functionality
# this should be implimented in logic


# TERMINAL ACCESS
__AUTH_ROOT   = _MappingProxyType({
    'login'   : _root.login,
    'password': _root.password,
})
__AUTH_ADMIN  = _MappingProxyType({
    'login'   : _admin.login,
    'password': _admin.password,
})


# DATABASES
# postgresql configs to work with libs.db.database
_postgers_conf = {
    'dialect'  : os.getenv('DB_DIALECT'),
    'driver'   : os.getenv('DB_DRIVER'),
    'username' : os.getenv('DB_USER'),
    'password' : os.getenv('DB_PASSWORD'),
    'host'     : os.getenv('DB_HOST'),
    'port'     : os.getenv('DB_PORT'),
    'database' : os.getenv('DB_NAME'),
}
# _DB_CONFIG = {**postgers_conf}  # by postgres
_DB_CONFIG = {}  # by SQLite


# MS OFFICE
# excel configurate to work with libs.excel
_EXCEL_CONFIG = {
    'file'   : _EXCEL_PATH,
    'engine' : 'openpyxl',
    'header' : None,
}


# MAILING
# Keep in mind that Gmail requires that you connect to port 465
# if using SMTP_SSL(), and to port 587 when using .starttls().
_smtp_ssl = 465
_starttls = 587
_smtp_port = _starttls
_smtp_host = 'smtp.gmail.com:'

class _Subject(dict):
    """Set subject emails."""
    default = 'Дамы и господа, дратути!'
    def __missing__(self, key): return self.default

_EMAIL_CONFIG = {
    'login' : os.getenv('EMAIL_LOGIN'),
    'pwd'   : os.getenv('EMAIL_PASSWORD'),
    'lvl'   : False,  # terminal logging
    'smtp'  : f'{_smtp_host} {_smtp_port}',
    'subj'  : '{msg}'.format_map(_Subject(
        # msg = add_subject__or__live_default
              ))
}


# FOR ALL COMPONENTS OFF THIS APP
# Экспорт в __main__ -> terminal -> protocol -> <package>
CONFIGS = c = _SimpleNamespace()
"""Configs for all components."""
c.argument = _ARGUMENT
c.packages = _FUNCTIONALITY
c.wsdl_url = _WSDL_URL
c.db_conf  = _DB_CONFIG
c.ex_conf  = _EXCEL_CONFIG
c.smtp     = _EMAIL_CONFIG
c.off      = _OFF

c.terminal         = lambda: None    # durex
c.terminal.__doc__ = """Auth: root, admin"""
c.terminal.root    = __AUTH_ROOT
c.terminal.admin   = __AUTH_ADMIN
# FIXME ?


# alternative
from typing import NamedTuple as _NamedTuple

class _Config(_NamedTuple):
    terminal : _func
    argument : int
    packages : frozenset
    wsdl_url : str
    db_conf  : dict
    ex_path  : dict
    smtp     : dict
    off      : dict

_config = _Config(
    terminal = lambda: {
        'root' : __AUTH_ROOT,
        'admin': __AUTH_ADMIN,
    },
    argument = _ARGUMENT,
    packages = _FUNCTIONALITY,
    wsdl_url = _WSDL_URL,
    db_conf  = _DB_CONFIG,
    ex_path  = _EXCEL_CONFIG,
    smtp     = _EMAIL_CONFIG,
    off      = _OFF,
)
