# mypy ignore lines: 1
"""
This module works with a SOAP server.

Classes
-------
SOAPClient
    Interacting with a SOAP server

"""
import logging as _logging
from typing import (
    Optional as _Optional,
    AnyStr as _AnyStr,
    final as _final,
    Type as _Type,
)

import requests as _requests
import zeep as _zeep

_T = _Optional[_Type[_zeep.Client]] 


@_final
class SOAPClient:
    """
    Interacting with a SOAP server.
       
    Methods
    -------
    `request(args, kwargs)` : main logic api

    """
    def __init__(self, wsdl: str) -> None:
        """
        Initialize a new Terminal instance.
        
        """
        self._logger = _logging.getLogger(__name__)
        self._client: _T = None
        self._wsdl = wsdl

    @property
    def client(self): return self._client
    
    def request(self, *args, **kwargs) -> _Optional[_AnyStr]:
        """
        Return SOAP server response.

        Parameters
        ----------
        agrs : tuple(soap_arg,)
        kwargs : {'wsdl': 'path to soap server'}

        Calls
        -----
        `_zeep.Client(wsdl=kwargs['wsdl'])` -> _zeep.Client instance
        `_start_service_hello_name(args[0])` -> Optional[AnyStr]

        Raises
        ------
        _requests.exceptions.ConnectionError
            SOAP server is not available

        Warnings
        --------
        `libs.exceptions.errors.I_Forgot_To_Start_SoapServer_Again`

        """
        param, = args
        try:
            self._client = _zeep.Client(wsdl=self._wsdl)
        except _requests.exceptions.ConnectionError as ext:
            from exceptions.errors import I_Forgot_To_Start_SoapServer_Again as facking  # type: ignore

            self._logger.error(f'  Error occurred ! {ext.args}')
            self._logger.warning(f'  Probable cause: {facking.warning}')
        else:
            self._logger.debug('  Successfully initialized SOAP client')
            return self._start_service_hello_name(param)

    def _start_service_hello_name(self, param: _AnyStr) -> _Optional[_AnyStr]:
        """
        Start my test service :)

        Parameters
        ----------
        param : AnyStr
            The argument passed to the remote procedure

        Calls
        -----
        `_client.service.hello_name(param)`

        """
        return self._client.service.hello_name(param)

    def __repr__(self):
        return f'{self.__class__.__name__} ({self._client=!r}'
    