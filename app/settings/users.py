
# XXX недоделано 

__all__ = ('_root', '_admin')

# from abc import ABC, abstractmethod
from typing import TypedDict as _TypedDict

# Если класс является подклассом другого класса и его поведение в основном унаследовано от этого класса, в его строке документации следует упомянуть об этом и резюмировать различия. Используйте глагол «переопределить», чтобы указать, что метод подкласса заменяет метод суперкласса и не вызывает метод суперкласса; используйте глагол «расширить», чтобы указать, что метод подкласса вызывает метод суперкласса (в дополнение к своему собственному поведению).
class _User:
    def __init__(self, **kwargs) -> None:
        kwargs.setdefault('login', None)
        kwargs.setdefault('password', None)
        self.__dict__.update(kwargs)
        self.session = False

    def __repr__(self):
        return (f'{self.__class__.__name__}('
                f'login={self.login}, password={self.password})')


class _Root(_User):
    def __init__(self, **kwargs) -> None:
        kwargs.setdefault('login', '')  #root)
        kwargs.setdefault('password', '')  #299_792_458)
        super().__init__(**kwargs)


class _TD(_TypedDict, total=False):
    root: set[str]
    admin: set[str]
    other: set[str]
    

class _Group:
    __access: _TD = {
        'root': {'root', 'sudo'},
        'admin': {'sudo'},
    }

    def __init__(self, value, lvl='other') -> None:
        self.value = value
        self.level = lvl

    @property
    def u_lvl(self):
        return _Group.__access[self.level]
 
_admin = _User()
_root = _Root()
