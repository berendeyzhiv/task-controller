"""
Errors module.

Classes
-------
`BaseError`
    Base class for other exceptions
`AuthorizationError`
    Terminal authorization
`I_Forgot_To_Start_SoapServer_Again`
    (-_-)
`UnexpectedKeywordArgumentError`
    DB URL configuration

"""


class BaseError(Exception):
    """A base class for other exceptions"""

class AuthorizationError(BaseError):
    """Raised when the login and/or password not correct"""

class I_Forgot_To_Start_SoapServer_Again(BaseException):
    """Raised when i forgot again"""
    warning = 'I forgot to start soap server agaaaaain?'

class  UnexpectedKeywordArgumentError(BaseException):
    """`_DatabaseURLSetter.__init__()` got an unexpected keyword arg"""


if __name__ == '__main__':
    import exceptions as _exceptions  # type: ignore

    print('digraph {')
    for name in dir(_exceptions):
        item = getattr(_exceptions, name)
        if isinstance(item, type) and BaseException in item.__mro__:
            for parent in item.__bases__:
                print(f'\t{parent.__name__} -> {item.__name__};')
    print('}')