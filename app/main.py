#!/usr/bin/env python3
# mypy ignore lines count: 1
"""
Here we get started.

Funtions
--------
`apply(num, term)`
    Run Terminal with a user-defined number or threads.

"""
import logging as _logging

from terminal import Terminal as _Term
from settings.base import CONFIGS  as _config  # type: ignore

_logger = _logging.getLogger(__name__)


def apply(num: int, term: _Term) -> None:
    """Launch  `Terminal`  in  `num`  threads."""
    for i in range(num):
        t = term.start()
        _logger.debug(f'  Decided to main for request: {i+1}')
        t.join()

if __name__ == '__main__':
    _logger.debug(f'  Main started with arg: {_config.argument}')

    _term: _Term = _Term(_config)
    _number_of_threads: int = _config.argument or 1
    apply(_number_of_threads, _term)

    _logger.debug('  End of main')
